import React from 'react';
import store from './src/store';
import { StoreProvider } from 'easy-peasy';

export default ({ element }) => (
  <StoreProvider store={store}>
    {element}
  </StoreProvider>
);
