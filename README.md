# **MDNT**
## Frontend project for MDNT

## **Git**

Main branches `prod`, `staging` and `development`.

We use short-lived feature branches like `feature/my-feature`

Use pull requests with reviewing when merging to prod.

## **Prerequisites**

* [Gatsby.js](https://gatsbyjs.org)
* [Yarn](https://yarnpkg.com/lang/en/)
* [Node](https://nodejs.org/en/)

## **Setting up**

* run `yarn` or `npm -i` to install project

## **Starting up**

* `yarn develop` to run development mode. Will start up on `http://localhost:8000`
* `yarn build` to build for production
* `yarn serve` to serve the production build locally

## **Deployment**

Deployment is being made to _____ when pushing to `prod` or `staging` branches.


